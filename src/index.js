import _ from 'lodash';
import $ from 'jquery';
let currentPoint;
function component() {
    
    $("#main").append("<div>in a div</div>");
    
    let element = document.createElement('div');
  
    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack', 'again'], ' ');
  
    return element;
  }
  function log(msg) {
    $("#log").prepend(msg+"\n");
  }
  function renderCoord({offsetX=222, offsetY=333, pageX,pageY,clientX="?",clientY="?",screenX=-5,screenY=-2,ctrlKey=false,altKey=false,shiftKey=false}) {
    return `(${offsetX},${offsetY}), ${pageX},${pageY}), (${clientX},${clientY}), (${screenX},${screenY}), ctrl=${ctrlKey}, alt=${altKey}, shiftKey=${shiftKey} TIME`;
  }
  function drawPoint({offsetX, offsetY}) {
    var ctx = document.getElementById('cp-canvas').getContext('2d');
    let side=5;
    log(`JJS ${offsetX-5}, ${offsetY-5}, ${side}, ${side}`);
    ctx.fillRect(offsetX-(side/2), offsetY-(side/2), side, side);
    ctx.beginPath();
    ctx.arc(offsetX, offsetY, side, 0, Math.PI * 2, true); 
    ctx.stroke();

  }
  $(document).ready(function() {
    $("#cp-canvas").mousedown(function(e){
      log("mousedown "+renderCoord(e));
      drawPoint(e)
    });
    $("#cp-canvas").mousemove(function(e){
      log("mousemove " + renderCoord(e));
    });
    $("#clear-button").click(function() {
      $("#log").html('');
      var canvas = document.getElementById('cp-canvas');
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    });
    //var ctx = $("#cp-canvas").getContext('2d');
    var ctx = document.getElementById('cp-canvas').getContext('2d');
    ctx.font = '5px';
    ctx.fillText('.', 10, 10);

  });
  
  document.body.appendChild(component());