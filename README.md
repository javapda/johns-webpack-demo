# johns-webpack-demo #

## purpose ##

* learning about webpackjs

# WOW #

* with mode in development: 854 KiB
* with mode in production: 157 KiB

# Product production #

npm start -- --mode production

# Development development #

npm start -- --mode development


## Scripts ##

### added elements to the scripts stanza in package.json ###
* webpack-version to show the currently installed version of webpack
* [webpack-help show command-line help and options](./docs/webpack-help.md)



# Links #

* [webpackjs](https://webpack.js.org)
* [getting started](https://webpack.js.org/guides/getting-started/) | [install](https://webpack.js.org/guides/installation/) | [tree shaking (remove dead code)](https://webpack.js.org/guides/tree-shaking/)

